<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>MARI CUCI INVOICE</title>
    <link rel="stylesheet" href="{{asset('assets/css/style_invoice.css')}}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Rubik&display=swap" rel="stylesheet">
</head>

<body>
    <html lang="en">

    <head>
        <meta charset='UTF-8'>
        <title>Editable Invoice</title>
    </head>

    <body>
        <div id="page-wrap">
            <textarea id="header">INVOICE</textarea>
            <div id="identity">
                <textarea id="address">Komplek Nusa Persada No.H3 Rt.03/ Rw.03 Kel. Leuwigajah Kec. Cimahi Selatan
Phone: (+62) 85624400593</textarea>
                <div id="logo">
                    <div id="logoctr">
                        <a href="javascript:;" id="change-logo" title="Change logo">Change Logo</a>
                        <a href="javascript:;" id="save-logo" title="Save changes">Save</a>
                        <a href="javascript:;" id="delete-logo" title="Delete logo">Delete Logo</a>
                        <a href="javascript:;" id="cancel-logo" title="Cancel changes">Cancel</a>
                    </div>
                    <div id="logohelp">
                        <input id="imageloc" type="text" size="50" value="" /><br />
                        (max width: 540px, max height: 100px)
                    </div>
                    <img id="image" style="height:50px"
                        src="https://www.flaticon.com/svg/static/icons/svg/3182/3182195.svg" alt="logo" />
                </div>
            </div>
            <div style="clear:both"></div>
            <div id="customer">
                <h1 id="customer-title">Mari Cuci Online</h1>
                <table id="meta">
                    <tr>
                        <td class="meta-head">Invoice #</td>
                        <td>{{ $data->id_trans }}</td>
                    </tr>
                    <tr>

                        <td class="meta-head">Date</td>
                        <td>{{$data->created_at}}</td>
                    </tr>
                    <tr>
                        <td class="meta-head">Amount Due</td>
                        <td>
                            <div class="due">Rp. {{$data->total}}</div>
                        </td>
                    </tr>

                </table>
            </div>
            <table id="items">
                <tr>
                    <th>Nama Customer</th>
                    <th>Paket</th>
                    <th>Harga Paket</th>
                    <th>Berat</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>

                <tr class="item-row">
                    <td class="item-name">{{$data->havePelanggan->nama}}</td>
                    <td class="description">{{$data->havePaket->nama}}</td>
                    <td class="description">Rp. {{$data->havePaket->harga}}</td>
                    <td>{{$data->berat}} Kg</td>
                    <td>1</td>
                    <td><span class="price">Rp. {{$data->total}}</span></td>
                </tr>
                <tr id="hiderow">
                    <td colspan="5"><a id="addrow" href="javascript:;" title="Add a row">Add a row</a></td>
                </tr>
                <tr id="hiderow">
                    <td colspan="5"><a id="addrow" href="javascript:;" title="Add a row">Add a row</a></td>
                </tr>
                <tr>
                    <td colspan="3" class="blank"> </td>
                    <td colspan="2" class="total-line">Denda</td>
                    <td class="total-value">
                        <div id="total">Rp. 0</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="blank"> </td>
                    <td colspan="2" class="total-line">Total</td>
                    <td class="total-value">
                        <div id="total">Rp.{{$data->total}}</div>
                    </td>
                </tr>
            </table>
            <br><br><br><br>
            <div id="terms">
                <h5>MARI CUCI ONLINE</h5>
                <textarea>ZIGMA ART CREATIVE DESIGN</textarea>
            </div>
        </div>
    </body>

    </html>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="{{asset('assets/js/index_invoce.js')}}"></script>
</body>

</html>