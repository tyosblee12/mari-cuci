<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MesinModel;
use App\User;
use App\PelangganModel;
use App\PaketModel;
use App\TransaksiModel;
use DB;

class DashboardController extends Controller
{
    public function index(){
        $data = MesinModel::all();
        $tugas = User::all();
        $plg = PelangganModel::all();
        $paket = PaketModel::all();
        $trans = TransaksiModel::all();
        return view ('admin.dashboard', compact('data','tugas','plg','paket','trans'));
    }

    public function transaksi(){
        $tugas = User::all();
        $trans = TransaksiModel::all();
        return view ('admin.laundry.index',compact('tugas','trans'));
    }
    
// SIMPAN TRANSAKSI
    public function simpan(Request $request, TransaksiModel $transaksimodel)
    {

            $harga = PaketModel::all()->where('id_paket', $request->id_paket);
            foreach ($harga as $row){
            }
            $hrg = $row->harga;
            $jumlah = $request->berat;
            $total = $hrg * $jumlah;

        $simpan = $transaksimodel->create([
            'id_trans'      => $request->id_trans,
            'id_user'       => $request->id_user,
            'id_plg'        => $request->id_plg,
            'id_paket'      => $request->id_paket,
            'berat'         => $request->berat,
            'total'         => $total,
            'is_active'         => 1,
            'created_at'    => $request->created_at,
            'status'        => $request->status,
        ]);

        if (!$simpan->exists) {
            return redirect()->route('dashboard')->with('error', 'data gagal disimpan');
        }
            return redirect()->route('dashboard')->with('success', 'Data berhasil disimpan');
    }

}