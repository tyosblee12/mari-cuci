<?php

namespace App\Http\Controllers;
use App\PelangganModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    public function index(){
        $tugas = User::all();
        $plg = PelangganModel::all()->where('is_active', '1');;
        return view ('admin.pelanggan.index', compact('plg','tugas'));
    }

    // TAMBAH DATA
    public function createData(){
        $plg = PelangganModel::all();
        $tugas = User::all();
        return view('admin.pelanggan.tambah', compact('tugas','plg'));
    }

// SIMPAN PLG REGIS
    public function postDataPlg(Request $request1, PelangganModel $PelangganModel)
    {
        $plg = PelangganModel::all();
        foreach ($plg as $row){
        }
        $idplg = $row->id_plg + 1;
        $simpan = $PelangganModel->create([
            'id_plg'        => $idplg,
            'nama'          => $request1->nama,
            'alamat'        => $request1->alamat,
            'telp'          => $request1->telp,
            'created_at'    => now(),
            'is_active'     => 1,
        ]);

        if (!$simpan->exists) {
            return redirect()->route('register_form')->with('error','Pendaftar Gagal');
        }
            return redirect()->route('register_form')->with('success','Pendaftar Berhasil');
    }

// SIMPAN
    public function postData(Request $request, PelangganModel $pelangganModel){

        $simpan = $pelangganModel->create([
            'id_plg'        => $request->id_plg,
            'nama'          => $request->nama,
            'alamat'        => $request->alamat,
            'telp'          => $request->telp,
            'created_at'    => now(),
            'is_active'     => 1,
        ]);

        if(!$simpan->exists){
            return redirect()->route('tampil_plg')->with('error','Data gagal disimpan');
        }

        return redirect()->route('tampil_plg')->with('success','Data {{$request->id_plg}} berhasil disimpan');
    }

// EDIT
    public function editData($id)
    {
        $tugas = User::all();
        $data = PelangganModel::where('id_plg', $id)->first();
        return view('admin.pelanggan.edit', compact('data','tugas'));
    }

// UPDATE
    public function updateData($id, PelangganModel $PelangganModel, Request $request)
    {
    
        $simpan = $PelangganModel->where('id_plg', $id)->update([
            'nama'         => $request->nama,
            'alamat'          => $request->alamat,
            'telp'         => $request->telp,
            'is_active'     => 1,
            'updated_at'    => now(),
        ]);
        
        if (!$simpan) {
            return redirect()->route('tampil_plg')->with('error', 'data gagal di update');
        }
    
        return redirect()->route('tampil_plg')->with('success', 'data berhasil di update');
    }

// DELETE
    public function softDelete($id, PelangganModel $PelangganModel)
    {
    
        $simpan = $PelangganModel->where('id_plg', $id)->update([
            'is_active' => '0',
        ]);
        
        if (!$simpan) {
            return redirect()->route('tampil_plg')->with('error', 'data gagal di dihapus');
        }
    
        return redirect()->route('tampil_plg')->with('success', 'data berhasil di hapus');
    }

}