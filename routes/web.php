<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// LOGIN
Auth::routes();
Route::get('/login', 'AuthController@index' )->name('login');
Route::get('/register', 'AuthController@register' )->name('register_form');
Route::post('/signin', 'AuthController@sendLoginRequest')->name('ceklogin');
Route::get('/logout', 'AuthController@logout')->name('logout_action');
Route::get('/resi', 'AuthController@tampilResi')->name('tampilResi');
Route::get('/resi/cek/{id}', 'AuthController@hasilResi')->name('hasilResi');

// DASHBOARD
Route::get('/dashboard', 'DashboardController@index' )->name('dashboard');

// TRANSAKSI
Route::get('/transaksi', 'DashboardController@transaksi' )->name('transaksi');
Route::post('/transaksi/simpan', 'DashboardController@simpan' )->name('simpan_transaksi');
Route::get('/transaksi/cetak/{id}', 'TransaksiController@cetak' )->name('cetak_invoice');
Route::get('/transaksi/edit/{id}', 'TransaksiController@editData' )->name('edit_transaksi');

// PELANGGAN
Route::get('/pelanggan', 'PelangganController@index' )->name('tampil_plg');
Route::get('/pelanggan/tambah', 'PelangganController@createData' )->name('tambah_plg');
Route::post('/pelanggan/simpan', 'PelangganController@postData')->name('post_plg');
Route::post('/', 'PelangganController@postDataPlg')->name('post_plg_login');
Route::get('/pelanggan/edit/{id}', 'PelangganController@editData' )->name('edit_plg');
Route::post('/pelanggan/update/{id}', 'PelangganController@updateData')->name('update_plg');
Route::get('/pelanggan/hapus/{id}', 'PelangganController@softDelete')->name('delete_plg');

//CRUD MESIN
Route::get('/mesin', 'MesinController@index' )->name('tampil_mesin');
Route::get('/mesin/tambah', 'MesinController@tambah_mesin' )->name('tambah_mesin');
Route::post('/mesin/simpan', 'MesinController@postDataMesin')->name('post_mesin');
Route::get('/mesin/edit/{id}', 'MesinController@editData')->name('edit_mesin');
Route::post('/mesin/update/{id}', 'MesinController@updateData')->name('update_mesin');
Route::get('/mesin/hapus/{id}', 'MesinController@softDelete')->name('delete_mesin');