<?php

use Illuminate\Database\Seeder;

class PaketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paket')->insert(array(
            array(
                'id_paket'       => '991',
                'nama'          => 'Paket Hemat',
                'harga'           => 15000,
                'status'         => 'Promo',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            array(
                'id_paket'       => '992',
                'nama'          => 'Paket Lengkap',
                'harga'           => 18000,
                'status'         => 'Promo',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            array(
                'id_paket'       => '993',
                'nama'          => 'Paket Hade',
                'harga'           => 25000,
                'status'         => 'Aktif',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            ));
    }
}