<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no')}}">
    <title>Login Mari Cuci !</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset ('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset ('assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset ('assets/css/style.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset ('assets/images/favicon.png')}}" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="row w-100 m-0">
                <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
                    <div class="card col-lg-4 mx-auto">
                        <div class="card-body px-5 py-5">
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{session('error')}}
                            </div>
                            @endif
                            <h3 class="card-title text-left mb-3">CEK RESI</h3>
                            <form action="#" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <label for="merek" class="col-sm-3 col-form-label">ID Transaksi</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control text-white" name="merek" id="merek"
                                            placeholder="Masukan Nomor Transaksi" value="">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-warning text-dark mr-2 float-right">Cek</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
            </div>
            <!-- row ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset ('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset ('/assets/js/off-canvas.js')}}"></script>
    <script src="{{ asset ('/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{ asset ('/assets/js/misc.js')}}"></script>
    <script src="{{ asset ('/assets/js/settings.js')}}"></script>
    <script src="{{ asset ('/assets/js/todolist.js')}}"></script>
    <!-- endinject -->
</body>

</html>