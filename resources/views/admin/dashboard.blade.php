@extends('admin.layouts.master')
@section('content')
<div class="col-12 grid-margin stretch-card">
</div>
</div>
<!-- ATAS -->
<div class="row">
    <div class="col-xl-3 col-sm-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-9">
                        <div class="d-flex align-items-center align-self-start">
                            <h3 class="mb-0">$12.34</h3>
                            <p class="text-success ml-2 mb-0 font-weight-medium">+3.5%</p>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="icon icon-box-success ">
                            <span class="mdi mdi-arrow-top-right icon-item"></span>
                        </div>
                    </div>
                </div>
                <h6 class="text-muted font-weight-normal">Potential growth</h6>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-9">
                        <div class="d-flex align-items-center align-self-start">
                            <h3 class="mb-0">$17.34</h3>
                            <p class="text-success ml-2 mb-0 font-weight-medium">+11%</p>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="icon icon-box-success">
                            <span class="mdi mdi-arrow-top-right icon-item"></span>
                        </div>
                    </div>
                </div>
                <h6 class="text-muted font-weight-normal">Revenue current</h6>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-9">
                        <div class="d-flex align-items-center align-self-start">
                            <h3 class="mb-0">$12.34</h3>
                            <p class="text-danger ml-2 mb-0 font-weight-medium">-2.4%</p>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="icon icon-box-danger">
                            <span class="mdi mdi-arrow-bottom-left icon-item"></span>
                        </div>
                    </div>
                </div>
                <h6 class="text-muted font-weight-normal">Daily Income</h6>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-9">
                        <div class="d-flex align-items-center align-self-start">
                            <h3 class="mb-0">$31.53</h3>
                            <p class="text-success ml-2 mb-0 font-weight-medium">+3.5%</p>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="icon icon-box-success ">
                            <span class="mdi mdi-arrow-top-right icon-item"></span>
                        </div>
                    </div>
                </div>
                <h6 class="text-muted font-weight-normal">Expense current</h6>
            </div>
        </div>
    </div>
</div>
<!-- AKHIR ATAS -->
<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tambah Transaksi</h4>
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <p class="card-description">Input Data Baru <code>Transaksi</code>
                    <form class="forms-sample text-white" action="{{ route ('simpan_transaksi') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="merek" class="col-sm-3 col-form-label">Kode Transaksi</label>
                            <div class="col-sm-9">
                                @foreach ($trans as $row)
                                @endforeach
                                @php
                                $D = date('Ymd');
                                $now = $D + 1;
                                @endphp
                                <input type="text" class="form-control text-white" name="id_trans" id="id_trans"
                                    placeholder="ID TRANSAKSI" value="{{ $row -> id_trans + 1 }}" read-only>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="exampleInputMobile" class="col-sm-3 col-form-label">Staff ID</label>
                            <div class="col-sm-9">
                                @foreach ($tugas as $row)
                                <input type="text" class="form-control text-white" name="id_user" id="id_user"
                                    placeholder="Tipe Mesin" value="{{ $row-> id_user }}">
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="exampleInputMobile" class="col-sm-3 col-form-label">Customer ID</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-md text-white" name="id_plg" id="customer_id">
                                    @foreach($plg as $row)
                                    <option value="{{ $row -> id_plg }}">{{$row -> id_plg}} - {{$row -> nama}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="exampleInputMobile" class="col-sm-3 col-form-label">Paket ID</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-md text-white" name="id_paket" id="id_paket">
                                    @foreach($paket as $row)
                                    <option value="{{ $row -> id_paket }}">
                                        {{$row->nama}} - Rp. {{$row -> harga}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="merek" class="col-sm-3 col-form-label">Berat</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control text-white" name="berat" id="berat"
                                    placeholder="Satuan Berat Kg" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tgl_masuk" class="col-sm-3 col-form-label">Tanggal Masuk</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control text-white" name="created_at" id="created_at"
                                    value="<?php echo date('Y-m-d'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="col-sm-3 col-form-label ">Status</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-md text-white" name="status" id="status">
                                    <option value="Proses" set>Proses</option>
                                    <option value="Selesai">Selesai</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-dark mr-2 float-right">Batal</button>
                        <button type="submit" class="btn btn-primary mr-2 float-right">Simpan</button>
                    </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tabel Transaksi</h4>
                <p class="card-description"> List Transaksi Cuci <code>Boss</code>
                </p>
                <div class="table-responsive">
                    <table class="table text-white">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Transaksi</th>
                                <th>Admin</th>
                                <th>Nama Admin</th>
                                <th>Customer ID</th>
                                <th>Nama Customer</th>
                                <th>Paket ID</th>
                                <th>Nama Paket</th>
                                <th>Tanggal Masuk</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php

                            $i=1;
                            $a = 'info';
                            @endphp
                            @foreach($trans as $row)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $row-> id_trans }}</td>
                                <td>{{ $row-> id_user }}</td>
                                <td>{{ $row->haveUser->name }}</td>
                                <td>{{ $row-> id_plg }}</td>
                                <td>{{ $row->havePelanggan->nama }}</td>
                                <td>{{ $row-> id_paket }}</td>
                                <td>{{ $row->havePaket->nama }}</td>
                                <td>{{ $row-> created_at }}</td>
                                <td><label class="badge badge-{{ $a }}">{{ $row-> status }}</label></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tabel Transaksi</h4>
                <p class="card-description"> List Transaksi Cuci <code>Boss</code>
                </p>
                <div class="table-responsive">
                    <table class="table text-white">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Mesin</th>
                                <th>Merek</th>
                                <th>Tipe</th>
                                <th>Tahun</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php

                            $i=1;
                            $a = 'info';
                            @endphp
                            @foreach($data as $row)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $row-> id_mesin }}</td>
                                <td>{{ $row-> merek }}</td>
                                <td>{{ $row-> tipe }}</td>
                                <td>{{ $row-> tahun }}</td>
                                <td><label class="badge badge-{{ $a }}">{{ $row-> status }}</label></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- content-wrapper ends -->
@endsection