@extends('admin.layouts.master')
@section('content')

</div>
<div class="page-header">
    <h3 class="page-title"> Tabel Transaksi </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Basic tables</li>
        </ol>
    </nav>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tabel Transaksi</h4>
                <p class="card-description"> List Transaksi Cuci <code>Boss</code>
                </p>
                <div class="table-responsive">
                    <table class="table text-white table-hover">
                        <thead>
                            <tr>
                                <th>ID Transaksi</th>
                                <th>Nama Admin</th>
                                <th>Customer ID</th>
                                <th>Nama Customer</th>
                                <th>Nama Paket</th>
                                <th>Total</th>
                                <th>Tanggal Masuk</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $a = 'primary';
                            @endphp
                            @foreach($trans as $row)
                            <tr>
                                <td>{{ $row-> id_trans }}</td>
                                <td>{{ $row->haveUser->name }}</td>
                                <td>{{ $row-> id_plg }}</td>
                                <td>{{ $row->havePelanggan->nama }}</td>
                                <td>{{ $row->havePaket->nama }}</td>
                                <td>{{ $row->total }}</td>
                                <td>{{ $row-> created_at }}</td>
                                <td><label class="badge badge-{{ $a }}">{{ $row-> status }}</label></td>
                                <td>
                                    <a href="{{route('cetak_invoice', $row->id_trans)}}" class="btn btn-info"><i
                                            class="mdi mdi-printer btn-icon-append"></i></a>
                                    <a href="#" class="btn btn-warning"><i
                                            class="mdi mdi-pencil btn-icon-append"></i></a>
                                    <a href="#" class="btn btn-secondary"><i
                                            class="mdi mdi-cup btn-icon-append"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->
@endsection