<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(UserSeeder::class);
        $this->call(MesinSeeder::class);
        $this->call(PaketSeeder::class);
        $this->call(PelangganSeeder::class);
        $this->call(TransaksiSeeder::class);
    }
}