<?php

use Illuminate\Database\Seeder;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pelanggan')->insert(array(
            array(
                'id_plg'       => '851',
                'nama'          => 'Setyo Dwi Cahyo',
                'alamat'           => 'Jl. Ibrahim Adji No.95',
                'telp'         => 852456258,
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            array(
                'id_plg'       => '852',
                'nama'          => 'Saila Saadiah Amanatillah',
                'alamat'           => 'Jl. Kampus 2 No.19',
                'telp'         => 852456258,
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            array(
                'id_plg'       => '853',
                'nama'          => 'Anasya Adreena',
                'alamat'           => 'Jl. Kampus 9 No.89',
                'telp'         => 852456258,
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            ));
    }
}