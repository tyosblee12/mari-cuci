<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaketModel extends Model
{
    protected $table = 'paket';
    
    protected $fillable = [
        'id_paket', 'nama', 'harga', 'status', 'is_active', 'created_at', 'updated_at',
    ];

    public function hasManyTransaksi()
    {
        return $this->hasMany(TransaksiModel::class, 'id_trans', 'id_trans');
    }

}