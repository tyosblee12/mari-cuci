@extends('admin.layouts.master')
@section('content')

</div>
<div class="col-md-6 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Ubah Data Pelanggan</h4>
            <form class="forms-sample text-white" action="{{ route('update_plg',$data->id_plg) }}" method="post">
                @csrf
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="nama" id="nama"
                            placeholder="Merek mesin" value=" {{ $data->nama }}">
                    </div>
                </div>
                <div class=" form-group row ">
                    <label for=" exampleInputMobile" class="col-sm-3 col-form-label">Alamat</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="alamat" id="alamat"
                            placeholder="Tipe Mesin" value="{{ $data->alamat }}">
                    </div>
                </div>
                <div class=" form-group row ">
                    <label for=" exampleInputMobile" class="col-sm-3 col-form-label">Nomor Telepon</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="telp" id="telp"
                            placeholder="Tipe Mesin" value="{{ $data->telp }}">
                    </div>
                </div>
                <button type="button" class="btn btn-dark mr-2 float-right">Batal</button>
                <button type="submit" class="btn btn-primary mr-2 float-right">Simpan</button>
            </form>
        </div>
    </div>
</div>

@endsection