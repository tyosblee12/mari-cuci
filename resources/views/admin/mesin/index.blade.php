@extends('admin.layouts.master')
@section('content')

</div>
<div class="page-header">
  <h3 class="page-title"> Tabel Mesin Cuci </h3>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Tables</a></li>
      <li class="breadcrumb-item active" aria-current="page">Basic tables</li>
    </ol>
  </nav>
</div>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Tabel Mesin Cuci</h4>
        <p class="card-description"> List Mesin Cuci <code>Laundry</code>
        </p>
        <a href="{{ route ('tambah_mesin')}}" type="button" class="btn btn-primary btn-icon-text float-right">
          <i class="mdi mdi-file-check btn-icon-prepend"></i> Tambah Data </a>
        <div class="table-responsive">
          <table class="table text-white">
            <thead>
              <tr>
                <th>No</th>
                <th>ID Mesin</th>
                <th>Merek</th>
                <th>Tipe</th>
                <th>Tahun</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php

              $i=1;
              $a = 'info';
              @endphp
              @foreach($data as $row)
              <tr>
                <th scope="row">{{ $i++ }}</th>
                <td>{{ $row-> id_mesin }}</td>
                <td>{{ $row-> merek }}</td>
                <td>{{ $row-> tipe }}</td>
                <td>{{ $row-> tahun }}</td>
                <td><label class="badge badge-{{ $a }}">{{ $row-> status }}</label></td>
                <td>
                  <a href="{{route('edit_mesin', $row->id_mesin)}}" type="button" class="btn btn-warning btn-icon-text">
                    <i class=" mdi mdi-lead-pencil btn-icon-prepend"></i> Edit </a> &nbsp
                  <a href="{{route('delete_mesin', $row->id_mesin)}}" type="button"
                    class="btn btn-danger btn-icon-text">
                    <i class=" mdi mdi-delete btn-icon-prepend"></i> Hapus </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
@endsection