@extends('admin.layouts.master')
@section('content')

</div>
<div class="page-header">
    <h3 class="page-title"> Tabel Pelanggan </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pelanggan</li>
        </ol>
    </nav>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tabel Data Pelanggan</h4>
                <a href="{{route ('tambah_plg')}}" type="button" class="btn btn-primary btn-icon-text float-right">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i> Tambah Data </a>
                <p class="card-description"> List Pelanggan Mari Cuci <code>Boss</code>
                </p>
                <div class="table-responsive">
                    <table class="table text-white">
                        <thead>
                            <tr>
                                <th>ID Pelanggan</th>
                                <th>Nama Pelanggan</th>
                                <th>Alamat</th>
                                <th>No Telp</th>
                                <th>Tanggal Masuk</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plg as $row)
                            <tr>
                                <td>{{ $row-> id_plg }}</td>
                                <td>{{ $row-> nama }}</td>
                                <td>{{ $row-> alamat }}</td>
                                <td>{{ $row-> telp }}</td>
                                <td>{{ $row-> created_at }}</td>
                                <td>
                                    <a href="{{route('edit_plg', $row->id_plg)}}" class="btn btn-info mr-2">Edit</a>
                                    <a href="{{route('delete_plg', $row->id_plg)}}" class="btn btn-secondary">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->


@endsection