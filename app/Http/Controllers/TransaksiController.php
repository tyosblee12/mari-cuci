<?php

namespace App\Http\Controllers;
use App\TransaksiModel;
use App\User;
use Illuminate\Http\Request;

$tugas = User::all();

class TransaksiController extends Controller
{
    // EDIT
    public function editData($id)
    {
        $tugas = User::all();
        $data = TransaksiModel::where('id_trans', $id)->first();
        return view('admin.laundry.edit', compact('data','tugas'));
    }

    // CETAK INVOICE
    public function cetak($id){
        $tugas = User::all();
        $data = TransaksiModel::where('id_trans', $id)->first();
        return view('admin.laundry.invoice',compact('tugas','data'));
    }

}