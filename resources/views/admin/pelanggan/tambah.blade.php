@extends('admin.layouts.master')
@section('content')

</div>
<div class="col-md-6 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Data Pelanggan</h4>
            <form class="forms-sample text-white" action="{{ route ('post_plg') }}" method="post">
                @csrf
                <div class="form-group row">
                    <label for="id_plg" class="col-sm-3 col-form-label">ID Pelanggan</label>
                    <div class="col-sm-9">
                        @foreach ($plg as $row)
                        @endforeach
                        <input type="text" class="form-control text-white" name="id_plg" id="id_plg"
                            placeholder="ID Pelanggan" value="{{$row-> id_plg + 1}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Nama Pelanggan</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="nama" id="nama"
                            placeholder="Nama Pelanggan">
                    </div>
                </div>
                <div class="form-group row ">
                    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="alamat" id="alamat"
                            placeholder="Alamat">
                    </div>
                </div>
                <div class="form-group row ">
                    <label for="telp" class="col-sm-3 col-form-label">Nomor Telepon</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="telp" id="telp"
                            placeholder="Nomor Telepon">
                    </div>
                </div>
                <button type="button" class="btn btn-dark mr-2 float-right">Batal</button>
                <button type="submit" class="btn btn-primary mr-2 float-right">Simpan</button>
            </form>
        </div>
    </div>
</div>

@endsection