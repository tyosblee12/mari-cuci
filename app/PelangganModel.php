<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PelangganModel extends Model
{
    protected $table = 'pelanggan';
    
    protected $fillable = [
        'id_plg', 'nama', 'alamat', 'telp', 'status', 'is_active', 'created_at', 'updated_at',
    ];

    public function hasManyTransaksi()
    {
        return $this->hasMany(TransaksiModel::class, 'id_trans', 'id_trans');
    }
}