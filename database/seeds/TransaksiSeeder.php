<?php

use Illuminate\Database\Seeder;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaksi')->insert(array(
            array(
                'id_trans'      => 20201214,
                'id_user'       => 791701,
                'id_plg'        => 851,
                'id_paket'      => 991,
                'berat'         => 2,
                'total'         => 30000,
                'status'        => 'Proses',
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
                ),
            array(
                'id_trans'      => 20201215,
                'id_user'       => 791701,
                'id_plg'        => 852,
                'id_paket'      => 991,
                'berat'         => 2,
                'total'         => 30000,
                'status'        => 'Proses',
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
                ),
            ));
    }
}