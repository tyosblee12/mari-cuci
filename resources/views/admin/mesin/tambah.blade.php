@extends('admin.layouts.master')
@section('content')

</div>
<div class="col-md-6 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Data Mesin</h4>
            <form class="forms-sample text-white" action="{{ route ('post_mesin') }}" method="post">
                @csrf
                <div class="form-group row">
                    <label for="merek" class="col-sm-3 col-form-label">Merek Mesin</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="merek" id="merek"
                            placeholder="Merek mesin">
                    </div>
                </div>
                <div class="form-group row ">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Tipe</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control text-white" name="tipe" id="tipe"
                            placeholder="Tipe Mesin">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun</label>
                    <div class="col-sm-9">
                        <select class="form-control form-control-md text-white" name="tahun" id="tahun">
                            @foreach(range(date('Y')-20, date('Y')) as $y)
                            <option value="{{ $y }}">{{ $y }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputConfirmPassword2" class="col-sm-3 col-form-label ">Status</label>
                    <div class="col-sm-9">
                        <select class="form-control form-control-md text-white" name="status" id="status">
                            <option value="Aktif">Aktif</option>
                            <option value="Aktif">Non-Aktif</option>
                            <option value="Aktif">Perawatan</option>
                            <option value="Aktif">Baru</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark mr-2 float-right">Batal</button>
                <button type="submit" class="btn btn-primary mr-2 float-right">Simpan</button>
            </form>
        </div>
    </div>
</div>

@endsection