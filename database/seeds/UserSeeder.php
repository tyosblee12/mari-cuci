<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'id_user'   => 791701,
                'name'      => 'Setyo Dwi Cahyo',
                'email'     => 'tyo@gmail.com',
                'password'  => bcrypt('admin'),
            ),
        ));
    }
}