<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiModel extends Model
{
    protected $table = 'transaksi';
    
    protected $fillable = [
        'id_trans', 'id_user', 'id_plg', 'id_paket', 'berat', 'total', 'status','is_active', 'created_at', 'updated_at',
    ];

    // PAKET
    public function havePaket()
    {
        return $this->belongsTo(PaketModel::class, 'id_paket', 'id_paket');
    }

    // USER ADMIN
    public function haveUser()
    {
        return $this->belongsTo(User::class, 'id_user', 'id_user');
    }

    // PELANGGAN
    public function havePelanggan()
    {
        return $this->belongsTo(PelangganModel::class, 'id_plg', 'id_plg');
    }

    

}