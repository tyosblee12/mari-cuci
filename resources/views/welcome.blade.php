<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no')}}">
    <title>Mari Cuci</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset ('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset ('assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset ('assets/css/style.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset ('assets/images/favicon.png')}}" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="row m-0 w-100 ">
                <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
                    <div class="row">
                        <div class="col-md-12 p-5">
                            <div class="col-md-4">
                                <div style="font-size:8vw;" class="mb-5 font-weight-bolder h1 ">
                                    MARI CUCI !
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-5">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has
                                    been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a
                                    galley of type and scrambled it to make a type specimen book. It has survived not
                                    only five
                                    centuries.
                                </div>
                            </div>

                            <div class="col-md-12 float-left">
                                <a href="{{route ('tampilResi')}}" type="button"
                                    class="btn btn-warning btn-lg text-dark font-weight-bold badge-outline-light btn-icon-text mb-2">
                                    <i class="mdi mdi-credit-card-scan btn-icon-prepend"></i> CEK RESI </a> &nbsp

                                <a href="{{route ('register_form')}}" type="button"
                                    class="btn btn-secondary text-dark btn-lg badge-outline-light btn-icon-text mb-2">
                                    <i class=" mdi mdi-account-plus btn-icon-prepend"></i> Registrasi </a> &nbsp

                                <!-- <a href="{{route ('login')}}" type="button"
                                    class="btn btn-lg badge-outline-light btn-icon-text mb-2">
                                    <i class=" mdi mdi-account btn-icon-prepend"></i> Login </a>
                            </div> -->
                            </div>
                        </div>

                    </div>
                    <!-- content-wrapper ends -->
                </div>
                <!-- row ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="{{ asset ('/assets/vendors/js/vendor.bundle.base.js')}}"></script>
        <!-- endinject -->
        <!-- Plugin js for this page -->
        <!-- End plugin js for this page -->
        <!-- inject:js -->
        <script src="{{ asset ('/assets/js/off-canvas.js')}}"></script>
        <script src="{{ asset ('/assets/js/hoverable-collapse.js')}}"></script>
        <script src="{{ asset ('/assets/js/misc.js')}}"></script>
        <script src="{{ asset ('/assets/js/settings.js')}}"></script>
        <script src="{{ asset ('/assets/js/todolist.js')}}"></script>
        <!-- endinject -->
</body>

</html>