<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MesinModel extends Model
{
    protected $table = 'mesin';
    protected $fillable = [
        'id_mesin', 'tipe', 'merek', 'tahun', 'status', 'is_active', 'created_at', 'updated_at',
    ];
}