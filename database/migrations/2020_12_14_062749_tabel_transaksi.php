<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TabelTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->string('id_trans',15)->primary();
            $table->string('id_user',10);
            $table->foreign('id_user')->references('id_user')->on('users');
            $table->string('id_plg',10);
            $table->foreign('id_plg')->references('id_plg')->on('pelanggan');
            $table->string('id_paket',10);
            $table->foreign('id_paket')->references('id_paket')->on('paket');
            $table->string('berat',2);
            $table->string('total',12);
            $table->string('status',15);
            $table->string('is_active',2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}