<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\MesinModel;
use App\User;

class MesinController extends Controller
{
    public function index()
    {
        // $data = MesinModel::all();
        $tugas = User::all();
        $data = DB::table('mesin')
        ->where('is_active','1')
        ->get();
        return view('admin.mesin.index', compact('data','tugas'));

    }

// TAMBAH 
    public function tambah_mesin()
    {
        $mesin = MesinModel::all();
        $tugas = User::all();
        return view('admin.mesin.tambah', compact('mesin','tugas'));
    }

// SIMPAN 
    public function postDataMesin(Request $request, MesinModel $MesinModel)
    {

        $simpan = $MesinModel->create([
            'merek'         => $request->merek,
            'tipe'          => $request->tipe,
            'tahun'         => $request->tahun,
            'status'        => $request->status,
            'is_active'     => 1,
            'created_at'    => now(),
            'updated_at'    => now(),
        ]);

        if (!$simpan->exists) {
            return redirect()->route('tampil_mesin')->with('error', 'data gagal disimpan');
        }
        return redirect()->route('tampil_mesin')->with('success', 'data berhasil disimpan');
    }
    
// EDIT
    public function editData($id)
    {
        $tugas = User::all();
        $data = MesinModel::where('id_mesin', $id)->first();
        return view('admin.mesin.edit', compact('data','tugas'));
    }
    
// UPDATE
    public function updateData($id, MesinModel $MesinModel, Request $request)
    {

        $simpan = $MesinModel->where('id_mesin', $id)->update([
            'merek'         => $request->merek,
            'tipe'          => $request->tipe,
            'tahun'         => $request->tahun,
            'status'        => $request->status,
            'is_active'     => 1,
            'updated_at'    => now(),
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_mesin')->with('error', 'data gagal di update');
        }

        return redirect()->route('tampil_mesin')->with('success', 'data berhasil di update');
    }

// DELETE
    public function softDelete($id, MesinModel $MesinModel)
    {

        $simpan = $MesinModel->where('id_mesin', $id)->update([
            'is_active' => '0',
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_mesin')->with('error', 'data gagal di dihapus');
        }

        return redirect()->route('tampil_mesin')->with('success', 'data berhasil di hapus');
    }

}