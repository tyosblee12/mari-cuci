<?php

use Illuminate\Database\Seeder;

class MesinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mesin')->insert(array(
            array(
                'id_mesin'       => '101',
                'merek'          => 'Panasonic',
                'tipe'           => 'TX-991',
                'tahun'          => 2010,
                'status'         => 'Aktif',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            array(
                'id_mesin'       => '102',
                'merek'          => 'Toshiba',
                'tipe'           => 'MX-9200',
                'tahun'          => 2008,
                'status'         => 'Perawatan',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),
            array(
                'id_mesin'       => '103',
                'merek'          => 'Sharp',
                'tipe'           => 'ES-FL862',
                'tahun'          => 2001,
                'status'         => 'Aktif',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),    
            array(
                'id_mesin'       => '104',
                'merek'          => 'Samsung',
                'tipe'           => 'WD10K6410OX',
                'tahun'          => 2008,
                'status'         => 'Aktif',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),    
            array(
                'id_mesin'       => '105',
                'merek'          => 'Hilux',
                'tipe'           => 'EWT1212',
                'tahun'          => 2018,
                'status'         => 'Non-Aktif',
                'is_active'      => 1,
                'created_at'     => now(),
                'updated_at'     => now(),
                ),    
        ));
    }
}